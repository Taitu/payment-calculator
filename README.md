# Payment calculator

Install all the dependencies
```
  yarn
```

## Setup
* `cp .env.example .env` and make changes according for your needs

**RATE** is rate that you charge client

**ACCESS_TOKEN** Generate access token in your gitlab profile. https://prnt.sc/sig7y5

**USERNAME** Gitlab username

**PROJECT_ID** project id

**MILESTONE** milestone name


## Calculate
* `yarn calc`