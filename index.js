const fetch = require('node-fetch');
require('dotenv').config();

const secToHours = (seconds, rate) => {
  const rawH = seconds / 3600
  const h = Math.floor(rawH);
  const m = (seconds % 3600) / 60;

  return {
    human: `${h}h ${m}m`,
    charge: rawH * rate,
    rawH,
    seconds,
  };
}

const rate = process.env.RATE;
const accessToken = process.env.ACCESS_TOKEN;
const username = process.env.USERNAME;
const pojectUrl = `https://gitlab.com/api/v4/projects/${process.env.PROJECT_ID}`;
const milestone = process.env.MILESTONE

fetch(`${pojectUrl}/issues?status=closed&per_page=100&page=1&assignee_username=${username}&milestone=${milestone}`, {
  headers: { "PRIVATE-TOKEN": accessToken },
})
  .then(res => res.json())
  .then(issues => {
    const unPaidIssues = issues.filter(({ labels, state }) => !labels.includes('paid') && state === 'closed');
    console.log(issues[0])

    let totalTimeInSec = unPaidIssues.reduce((acc, curr) => {
      const spent = curr.time_stats.total_time_spent
      return acc + spent;
    }, 0);

    const totalCharge = secToHours(totalTimeInSec, rate);

    const table = unPaidIssues.map(issue => {
      const spend = secToHours(issue.time_stats.total_time_spent, rate);
      return {
        id: issue.iid,
        title: issue.title,
        charge: `$${spend.charge}`,
        time_spent: issue.time_stats.human_total_time_spent,
        url: issue.web_url
      }
    });

    const result = {
      table,
      total: {
        time_spent: totalCharge.human,
        charge: `$${totalCharge.charge}`,
        rate: `$${rate} per hour`,
      }
    }

    console.log(JSON.stringify(result, null, 2))

  });
